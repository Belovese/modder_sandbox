# Modder's Sandbox

Based on **Bare Geomod**, with all scripts deactivated, this sandbox aims it to provide a testing ground for modders and a way to easily share our results and/or replicate others.

## How to

**Make a test:**
- simply create a new branch and do all testing there. 
- A good practice would be to explain your test and present your results in the `readme.md` of that branch. 
- Then you just have to share the link to that particular branch with anyone you'd like.

**Try somebody else's test:**
- go to that branch
- download or clone the sandbox in that particular branch

**Contribute:**
- either you know git and I have nothing to explain to you
- either you don't know git, then reach out to me (Belovèse) in TWcenter or Discord

## Credits
This is just a version of Bare Geomod, made by Gigantus, [see original on TWcenter](http://www.twcenter.net/forums/showthread.php?p=3894857#post3894857)