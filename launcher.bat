@echo off
title Gig's Launcher
cd ..\..
IF EXIST kingdoms.exe (start /affinity 1 /high kingdoms.exe @%0\..\configuration.cfg) ELSE (
IF EXIST medieval2.exe (start /affinity 1 /high medieval2.exe @%0\..\configuration.cfg) ELSE (
    echo.
    echo ERROR: Cannot find the M2TW or Kingdoms executable.
    echo.
    pause
  )
)

